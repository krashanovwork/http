﻿using System.Net;

namespace Listener
{
    internal static class ListenerClass
    {
        #region Task 1
        public static void NameParserListener(string[] prefixes)
        {
            if (prefixes is null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            using HttpListener httpListener = new HttpListener();

            foreach (string prefix in prefixes)
            {
                httpListener.Prefixes.Add(prefix);
            }
            httpListener.Start();

            Console.WriteLine("Server is listening its clients");

            while (true)
            {
                HttpListenerContext context = httpListener.GetContext();
                HttpListenerResponse response = context.Response;

                Console.WriteLine("Client connected");

                if (context.Request.Url.AbsolutePath == "/MyName")
                {
                    string responseString = GetMyName();

                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);

                    response.ContentLength64 = buffer.Length;
                    Stream output = response.OutputStream;
                    output.Write(buffer, 0, buffer.Length);
                    output.Close();
                }
                else
                {
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    response.Close();
                }
            }
        }
        #endregion

        #region Task 2
        public static void StatusCodeListener(string[] prefixes)
        {
            if (prefixes is null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            using HttpListener httpListener = new HttpListener();

            foreach (string prefix in prefixes)
            {
                httpListener.Prefixes.Add(prefix);
            }
            httpListener.Start();

            Console.WriteLine("Server is listening its clients");

            while (true)
            {
                HttpListenerContext context = httpListener.GetContext();

                using (var response = context.Response)
                {
                    string absolutePath = context.Request.Url.AbsolutePath;

                    if (absolutePath == "Information")
                        response.StatusCode = (int)HttpStatusCode.EarlyHints;
                    else if (absolutePath == "/Success")
                    {
                        response.StatusCode = (int)HttpStatusCode.OK;
                        Console.WriteLine("Client connected");
                    }
                    else if (absolutePath == "/Redirection")
                        response.StatusCode = (int)HttpStatusCode.Redirect;
                    else if (absolutePath == "/ClientError")
                        response.StatusCode = (int)HttpStatusCode.NotFound;
                    else if (absolutePath == "/ServerError")
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    else
                        response.StatusCode = (int)HttpStatusCode.BadGateway;

                    response.Close();
                }
            }
        }
        #endregion

        #region Task 3
        public static void GetName(string[] prefixes)
        {
            if (prefixes is null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            using HttpListener httpListener = new HttpListener();
            foreach (string prefix in prefixes)
            {
                httpListener.Prefixes.Add(prefix);
            }
            httpListener.Start();
            while (true)
            {
                HttpListenerContext context = httpListener.GetContext();
                Console.WriteLine("Client connected task3");

                using (var response = context.Response)
                {
                    string absolutePath = context.Request.Url.AbsolutePath;

                    if (absolutePath == "/MyNameByHeader")
                    {
                        GetMyNameByHeader(response);

                    }
                }

            }
        }
        public static void GetMyNameByHeader(HttpListenerResponse httpListenerResponse)
        {
            httpListenerResponse.AddHeader("X-MyName", GetMyName());
            httpListenerResponse.Close();
        }

        #endregion

        #region Task 4
        public static void GetNameByCookies(string[] prefixes)
        {
            using HttpListener httpListener = new HttpListener();
            foreach (string prefix in prefixes)
            {
                httpListener.Prefixes.Add(prefix);
            }
            httpListener.Start();
            while (true)
            {
                HttpListenerContext context = httpListener.GetContext();
                Console.WriteLine("Client connected");

                using (var response = context.Response)
                {
                    string absolutePath = context.Request.Url.AbsolutePath;

                    if (absolutePath == "/MyNameByCookies")
                    {
                        GetMyNameByCookies(response);
                    }
                }

            }
        }
        static void GetMyNameByCookies(HttpListenerResponse httpListenerResponse)
        {
            var myNameCookie = new Cookie("MyName", GetMyName());
            httpListenerResponse.Cookies.Add(myNameCookie);
            httpListenerResponse.Close();
        }
        #endregion
        static string GetMyName() => "Oleksandr";
    }
}
