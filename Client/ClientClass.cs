﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    internal static class ClientClass
    {
        public static void GetMyName()
        {
            using var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://localhost:8888/");

            var response = httpClient.GetAsync("/MyName").Result;

            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
        }

        public static async Task GetStatusMessages()
        {
            string[] urls = { "Information", "Success", "Redirection", "ClientError", "ServerError" };

            using var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://localhost:8888/");

            foreach (var url in urls)
            {
                Console.WriteLine($"Requesting {url}");
                var response = await httpClient.GetAsync(url);
                Console.WriteLine($"Response status code: {response.StatusCode}");
            }
        }

        public static async Task GetHeaderAsync()
        {
            using var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://localhost:8888/");

            var response = await httpClient.GetAsync("MyNameByHeader");

            var myName = response.Headers.GetValues("X-MyName")?.First();

            Console.WriteLine($"My name is {myName}");

            Console.ReadKey();
        }

        public static async Task GetCookiesAsync()
        {
            using var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://localhost:8888/");

            var response = await httpClient.GetAsync("MyNameByCookies");

            var myName = (response.Headers.GetValues("Set-Cookie")?.First()).Split('=')[1];

            Console.WriteLine($"My name is {myName}");

            Console.ReadKey();
        }
    }
}
